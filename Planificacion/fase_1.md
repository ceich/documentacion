# Fase 1

> Este documento forma parte de la [planificación general](../general.md), la cual trata solo los objetivos prácticos del proyecto. Por otra parte, la [planificación paralela]() refiere a los objetivos académicos del mismo.


> La planificación a largo plazo de la página web del centro de estudiantes consiste de tres fases, de las cuales aquí se describe en detalle la primera.


La primer fase de desarrollo de la página web del centro de estudiantes de FICH (de ahora en más, `la página`), abarca el diseño de un flujo de trabajo íntegro, la estipulación de estándares de diseño y código para el desarrollo de `la página`, la creación de una librería con los componentes básicos a utilizarse en el resto del proyecto, y el desarrollo y puesta en producción del sistema digital de becas del centro de estudiantes.

De las tres fases, esta es la más tediosa de desarrollar, ya que tiene mucho `código invisible`, es decir, mucho de lo que hay que hacer en esta fase parecería no aportar nada por sí mismo, pues son cosas que no terminan teniendo ninguna interfaz de usuario. Sin embargo, esta fase es el pilar de las otras dos. Es muy importante que esta fase se realice con mucho cuidado, y se preste mucha atención al correcto diseño. Un error grave en esta fase, que llegue hasta la siguiente, podría no ser viable de corregir.

#
## Objetivos
Los objetivos de esta fase deben entenderse como un logro parcial, a darse junto a los de la [fase 1 de la planificación paralela]().

### Automatizar la asignación de becas
Si bien el sistema de asignación de becas actualmente se encuentra informatizado, aún existen procesos manuales que deberían estar automatizados. El sistema de becas debería permitir a un usuario del centro consultar la información de las inscripciones, y asignar los resultados de las becas, asegurando que solo llegue al usuario del centro información válida (Según reglas de negocio preestablecidas).

El proceso de asignación de becas tendría que ser intuitivo al usuario, permitiendo así que un usuario centro gestione la asignación de becas incluso si no conoce los pormenores sobre el control y validación de las postulaciones.


### Brindar nuevos servicios al becado
El tener una página pone a disposición del usuario la potencia de un sistema informático. Esto permite pensar en nuevos servicios hacia los becados, como es el caso de las becas de transporte, y la posibilidad de acreditar a través de la página el monto de la beca hacia la sube del alumno. Tomar este caso como ejemplo pragmático, aunque no excluyente.


### Transparencia del sistema de becas
El sistema de becas debe permitir a un alumno ver información actualizada sobre el estado de sus becas, tanto actual como pasadas.

¿Debería mostrar el balance de las becas?


### Primer acercamiento alumno - página
Esta fase es una oportunidad para probar todos los aspectos de la página sobre un conjunto de alumnos acotado (los becados). Esto debe aprovecharse para probar tanto los requisitos técnicos de la página, como su difusión, evaluando tanto la llegada como la retención de nuevos usuarios. Al ser una facultad chica, esto podría determinar el alcance de [la fase dos paralela]().

A su vez, el sistema de becas aporta una gran cantidad de datos de alumnos/usuarios.


### Preparar el camino para la fase dos
En conjunto con lo mencionado en el punto anterior, esta primer fase debe sentar las bases sobre las que desarrollar la fase dos.


## Vías
### Diseño del Workflow
### Estandarización
### Librería de componentes
### Sistema de Becas


## Roles de Usuario

### Alumno
#### Creación Y Verificación
#### Becados

### Centro
#### Creación

### Administrador de Beca
#### Administrador de Fotocopiadora
##### Creación Y Validación

#### Administrador de Comedor
##### Creación Y Validación

## Requisitos
### Becas de Fotocopiadora
### Becas de Transporte
### Becas de Cantina
### Becas de Comedor


