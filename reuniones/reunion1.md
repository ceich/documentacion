# Primera Reunión

## Objetivos de la reunion

- Introducir el proyecto (fase 1)
- Mencionar otras fases
- Ver quienes se suman
- Aclarar responsabilidades y que se espera de cada uno
- Explicar en breve la arquitectura de una aplicacion web

## Proyecto - Fase 1

Va a ser una aplicacion web, para digitalizar el sistema de becas del CEICH

Que becas?
- fotocopiadora
- cantina
- comedor
- transporte

### Usuarios
#### Alumnos

- Pueden ver el estado de sus becas (pasadas o presentes), no las gestionan, pero pueden renunciar a ellas
- Tambien se pueden postularse a nuevas becas (subiendo la documentacion correspondiente)

#### Centro

- Abren convocatiorias (periodo) a becas
- Gestionan las postulaciones, becas y becados
- Acceso al registro de becas

#### Administrador de beca

Actualizan el estado de las becas que les corresponden, son, por ejemplo, los que anotan cuantas copias saco un alumno

### Tecnologías y herramientas
- Neutrino
- Git
- GitLab

#### Front-end
- React
- Formik
- Javascript
- ServiceWorkers (offline)

#### Back-end
- NodeJS
- Typescript
- Express

#### Persistencia
- PostgreSQL

#### Hosting
- En veremos

#### Deploy
- Surge
- Kubernetes


